# SEO
此项目库整理了关于搜索引擎优化SEO教程资料。SEO（Search Engine Optimization），汉译为搜索引擎优化，是较为流行的网络营销方式及NNT，主要目的是增加特定关键字的曝光率以增加网站的能见度，进而增加销售的机会。通过对网站的SEO能够增大对搜索引擎的友好，进而为网站带来流量。
## SEO是什么？
![](https://gitlab.com/Library-Administrator/Library-Administrator-Img/SEO-Img/raw/master/README%20Matching/img01.jpg)
SEO（Search Engine Optimization）：汉译为搜索引擎优化。是一种方式:利用搜索引擎的规则提高网站在有关搜索引擎内的自然排名。目的是：为网站提供生态式的自我营销解决方案，让其在行业内占据领先地位，获得品牌收益；SEO包含站外SEO和站内SEO两方面；为了从搜索引擎中获得更多的免费流量，从网站结构、内容建设方案、用户互动传播、页面等角度进行合理规划，还会使搜索引擎中显示的网站相关信息对用户来说更具有吸引力。